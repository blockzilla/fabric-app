import PropTypes from "prop-types"
import React, { useEffect } from "react"

// MetisMenu
import MetisMenu from "metismenujs"
import { withRouter } from "react-router-dom"
import { Link } from "react-router-dom"

//i18n
import { withTranslation } from "react-i18next"

const SidebarContent = props => {
  // Use ComponentDidMount and ComponentDidUpdate method symultaniously
  useEffect(() => {
    const pathName = props.location.pathname

    const initMenu = () => {
      new MetisMenu("#side-menu")
      let matchingMenuItem = null
      const ul = document.getElementById("side-menu")
      const items = ul.getElementsByTagName("a")
      for (let i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i]
          break
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem)
      }
    }
    initMenu()
  }, [props.location.pathname])

  function activateParentDropdown(item) {
    item.classList.add("active")
    const parent = item.parentElement

    const parent2El = parent.childNodes[1]
    if (parent2El && parent2El.id !== "side-menu") {
      parent2El.classList.add("mm-show")
    }

    if (parent) {
      parent.classList.add("mm-active")
      const parent2 = parent.parentElement

      if (parent2) {
        parent2.classList.add("mm-show") // ul tag

        const parent3 = parent2.parentElement // li tag

        if (parent3) {
          parent3.classList.add("mm-active") // li
          parent3.childNodes[0].classList.add("mm-active") //a
          const parent4 = parent3.parentElement // ul
          if (parent4) {
            parent4.classList.add("mm-show") // ul
            const parent5 = parent4.parentElement
            if (parent5) {
              parent5.classList.add("mm-show") // li
              parent5.childNodes[0].classList.add("mm-active") // a tag
            }
          }
        }
      }
      return false
    }
    return false
  }

  return (
    <React.Fragment>
      <div id="sidebar-menu">
        <ul className="metismenu list-unstyled" id="side-menu">
          <li className="menu-title">{props.t("Asia Tech")} </li>

          <li>
            <Link to="/create-shipment" className="waves-effect">
              <i className="bx bx-list-ul"></i>
              <span>{props.t("Create Shipment")}</span>
            </Link>
          </li>

          <li>
            <Link to="/export-wizard" className="waves-effect">
              <i className="bx bx-export"></i>

              <span>{props.t("Export Wizard")}</span>
            </Link>
          </li>

          <li className="menu-title">{props.t("Shenzhen Shipping")}</li>

          <li>
            <Link to="/shipment-manager" className="waves-effect">
              <i className="bx bxs-ship"></i>

              <span>{props.t("Shipment Manager")}</span>
            </Link>
          </li>

          <li className="menu-title">{props.t("Euro Audio Visual")}</li>

          <li>
            <Link to="/track-shipment" className="waves-effect">
              <i className="bx bx-transfer"></i>

              <span>{props.t("Track Shipment")}</span>
            </Link>
          </li>
          <li>
            <Link to="/shipment-details" className="waves-effect">
              <i className="bx bx-home-circle"></i>

              <span>{props.t("View Shipments")}</span>
            </Link>
          </li>
        </ul>
      </div>
    </React.Fragment>
  )
}

SidebarContent.propTypes = {
  location: PropTypes.object,
  t: PropTypes.any,
}

export default withRouter(withTranslation()(SidebarContent))
