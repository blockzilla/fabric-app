import React, { useState, useEffect, useRef } from "react"
import MetaTags from "react-meta-tags"

import {
  Card,
  CardBody,
  Col,
  Container,
  Button,
  FormGroup,
  Input,
  Label,
  NavItem,
  NavLink,
  Progress,
  Row,
  TabContent,
  TabPane,
} from "reactstrap"
import { AvForm, AvField } from "availity-reactstrap-validation"
import { useHistory } from "react-router-dom"

import classnames from "classnames"
import { Link } from "react-router-dom"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

const ShipmentManager = () => {
  const [shipment, setShipment] = useState("true")
  const [activeTabProgress, setactiveTabProgress] = useState(3)
  const [progressValue, setprogressValue] = useState(25)
  const [activeTabVartical, setoggleTabVertical] = useState(3)
  const history = useHistory()
  const ws = useRef(null)
  function toggleTabVertical(tab) {
    if (activeTabVartical !== tab) {
      if (tab >= 3 && tab <= 11) {
        setoggleTabVertical(tab)
      }
    }
  }

  useEffect(() => {
    let storageVal = JSON.parse(localStorage.getItem("shipment"))

    if (storageVal === null || storageVal.recipient === undefined) {
      history.push("/create-shipment")
    } else {
      setShipment(storageVal)
      if (storageVal.currentState <= 11) {
        setoggleTabVertical(storageVal.currentState)
      }
    }
    connect()
  }, [])

  function connect() {
    ws.current = new WebSocket(`ws://0.0.0.0:8081`)

    ws.current.onerror = function (err) {
      console.error("Socket encountered error: ", err.message, "Closing socket")
      ws.current.close()
    }
    ws.current.onopen = function () {
      console.log("WebSocket connection established")
    }
    ws.current.onclose = function (e) {
      console.log(
        "Socket is closed. Reconnect will be attempted in 1 second.",
        e.reason
      )
      setTimeout(function () {
        connect()
      }, 1000)
    }

    ws.current.onmessage = function (event) {
      const data = JSON.parse(event.data)
      const payload = data.payload
      console.info(data.payload)
      console.info(data.payload)
      console.info(data.payload)

      if (data.type === "success") {
        handlePayload(payload)
      } else if (data.type === "failure") {
        console.error(data.payload)
      }
    }
  }

  function handlePayload(payload) {
    console.info(payload)
    localStorage.removeItem("shipment")
    localStorage.setItem("shipment", JSON.stringify(payload))
    setShipment(payload)
    if (payload.currentState <= 11) {
      setoggleTabVertical(payload.currentState)
    }
  }

  async function loading(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("loading", values)
    } else {
      console.error(errors)
    }
  }

  async function in_transit(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("in_transit", values)
    } else {
      console.error(errors)
    }
  }

  async function unloading(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("unloading", values)
    } else {
      console.error(errors)
    }
  }

  async function import_customs(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("import_customs", values)
    } else {
      console.error(errors)
    }
  }

  async function import_duties(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("import_duties", values)
    } else {
      console.error(errors)
    }
  }

  async function loading_carrige(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("loading_carrige", values)
    } else {
      console.error(errors)
    }
  }

  async function unloading_destination(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("unloading_destination", values)
    } else {
      console.error(errors)
    }
  }

  async function sendEvent(type, values) {
    var json = {
      type: type,
      payload: {
        recipient: shipment.recipient,
        consignmentNumber: shipment.consignmentNumber,
        remark: values.remark,
        intermediary: values.intermediary,
      },
    }
    if (values.duties !== null) {
      json.payload.duties = values.duties
    }
    ws.current.send(JSON.stringify(json))
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Breadcrumbs title="Shipment Manager" breadcrumbItem="Manager" />

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <div className="vertical-wizard wizard clearfix vertical">
                    <div className="steps clearfix">
                      <ul>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 3,
                          })}
                        >
                          <NavLink
                            className={classnames({
                              active: activeTabVartical === 3,
                            })}
                          >
                            <span className="number">1.</span> Loading Cargo
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 4,
                          })}
                        >
                          <NavLink
                            className={classnames({
                              active: activeTabVartical === 4,
                            })}
                          >
                            <span className="number">2.</span>{" "}
                            <span>In Transit</span>
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 5,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 5,
                              }),
                              "")
                            }
                          >
                            <span className="number">3.</span> Unloading
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 6,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 6,
                              }),
                              "")
                            }
                          >
                            <span className="number">4.</span> Import Customs
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 7,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 7,
                              }),
                              "")
                            }
                          >
                            <span className="number">5.</span> Import Duties
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 8,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 8,
                              }),
                              "")
                            }
                          >
                            <span className="number">6.</span> Loading Carrige
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 9,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 9,
                              }),
                              "")
                            }
                          >
                            <span className="number">7.</span> Unloading
                            Destination
                          </NavLink>
                        </NavItem>
                        <NavItem
                          className={classnames({
                            current: activeTabVartical === 10,
                          })}
                        >
                          <NavLink
                            className={
                              (classnames({
                                active: activeTabVartical === 10,
                              }),
                              "done")
                            }
                          >
                            <span className="number">8.</span> Shipped
                          </NavLink>
                        </NavItem>
                      </ul>
                    </div>
                    <div className="content clearfix">
                      <TabContent
                        activeTab={activeTabVartical}
                        className="body"
                      >
                        <TabPane tabId={3}>
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bx-upload text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Shipment Loading</h5>

                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={loading}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={4}>
                          {" "}
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bxs-ship text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>In Transit</h5>
                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={in_transit}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={5}>
                          {" "}
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bx-download text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Unloading Cargo</h5>

                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={unloading}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={6}>
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bx-check-circle text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Import Customs</h5>
                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={import_customs}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={7}>
                          {" "}
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bx-money text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Import Duties</h5>
                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={import_duties}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input1211">
                                            Enter Duties Total{" "}
                                          </Label>
                                          <AvField
                                            type="number"
                                            name="duties"
                                            className="form-control"
                                            id="basicpill-cardno-input1211"
                                          />
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={8}>
                          {" "}
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bx-upload text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Loading for Delievery</h5>

                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={loading_carrige}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={9}>
                          {" "}
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="bx bxs-truck text-success display-4" />
                                </div>
                                <div>
                                  <Row className="text-center">
                                    <Col>
                                      <h5>Loading shipment for delievery</h5>
                                      <AvForm
                                        className="needs-validation"
                                        onSubmit={unloading_destination}
                                      >
                                        <FormGroup>
                                          <Label for="basicpill-cardno-input121">
                                            Enter Remark{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="remark"
                                            className="form-control"
                                            id="basicpill-cardno-input121"
                                          />
                                          <Label for="intermediary">
                                            Enter Intermediary{" "}
                                          </Label>
                                          <AvField
                                            type="text"
                                            name="intermediary"
                                            className="form-control"
                                            id="intermediary"
                                            readOnly={true}
                                            value="Shenzhen Shipping"
                                          />
                                          <br />
                                          <Button
                                            color="primary"
                                            className="btn btn-primary waves-effect waves-light"
                                          >
                                            Process
                                          </Button>
                                        </FormGroup>
                                      </AvForm>
                                    </Col>
                                  </Row>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                        <TabPane tabId={10}>
                          <div className="row justify-content-center">
                            <Col lg="6">
                              <div className="text-center">
                                <div className="mb-4">
                                  <i className="mdi mdi-check-circle-outline text-success display-4" />
                                </div>
                                <div>
                                  <h5>Shipment Complete</h5>
                                  <p className="text-muted">
                                    Shipment #{shipment.consignmentNumber} has
                                    been delievered
                                  </p>
                                </div>
                              </div>
                            </Col>
                          </div>
                        </TabPane>
                      </TabContent>
                    </div>
                    <div className="actions clearfix">
                      <ul>
                        <li
                          className={
                            activeTabVartical === 3
                              ? "previous disabled"
                              : "previous"
                          }
                        ></li>
                        <li
                          className={
                            activeTabVartical === 10 ? "next disabled" : "next"
                          }
                        >
                          <Link
                            to="#"
                            onClick={() => {
                              toggleTabVertical(activeTabVartical + 1)
                            }}
                          >
                            Next
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default ShipmentManager
