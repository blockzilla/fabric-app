import React, { useState, useEffect, useRef } from "react"

import { Container, Row, Col, Card, CardBody, CardTitle } from "reactstrap"

const RecieveGoods = props => {
  const [statuses, setStatuses] = useState(true)
  const ws = useRef(null)

  useEffect(() => {
    connect()
  }, [])

  function connect() {
    ws.current = new WebSocket(`ws://0.0.0.0:8080`)

    ws.current.onerror = function (err) {
      console.error("Socket encountered error: ", err.message, "Closing socket")
      ws.current.close()
    }
    ws.current.onopen = function () {
      console.log("WebSocket connection established")
    }
    ws.current.onclose = function (e) {
      console.log(
        "Socket is closed. Reconnect will be attempted in 1 second.",
        e.reason
      )
      setTimeout(function () {
        connect()
      }, 1000)
    }

    ws.current.onmessage = function (event) {
      const data = JSON.parse(event.data)

      console.log(data)

      if (data.type === "statuses") {
        setStatuses(data.payload)
      } else {
      }
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <CardTitle className="mb-5">Shipment Timeline</CardTitle>
                  <div className="">
                    <ul className="verti-timeline list-unstyled">
                      {/* Render Horizontal Timeline Events */}
                      {statuses.length &&
                        statuses.map((status, key) => (
                          <li key={key} className="event-list">
                            <div className="event-timeline-dot">
                              <i
                                className={
                                  status.id === 3
                                    ? "bx bx-right-arrow-circle bx-fade-right"
                                    : "bx bx-right-arrow-circle"
                                }
                              />
                            </div>
                            <div className="media">
                              <div className="mr-3">
                                <i
                                  className={
                                    "bx " +
                                    status.iconClass +
                                    " h2 text-primary"
                                  }
                                />
                              </div>
                              <div className="media-body">
                                <div>
                                  <h5>{status.intermediary}</h5>
                                  <p className="text-muted">{status.remarks}</p>
                                </div>
                              </div>
                            </div>
                          </li>
                        ))}
                    </ul>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default RecieveGoods
