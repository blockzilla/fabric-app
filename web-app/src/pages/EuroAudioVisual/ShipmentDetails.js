import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Card, CardBody, Col, Container, Row, Table } from "reactstrap"
import { isEmpty, map } from "lodash"
import { useHistory } from "react-router-dom"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

//Import Image
import logo from "../../assets/images/logo-dark.png"

const ShipmentDetails = props => {
  const [shipment, setShipment] = useState("true")
  const history = useHistory()

  useEffect(() => {
    let storageVal = JSON.parse(localStorage.getItem("shipment"))

    if (storageVal === null || storageVal.recipient === undefined) {
      history.push("/create-shipment")
    } else {
      setShipment(storageVal)
    }
  }, [])

  const printInvoice = () => {
    window.print()
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Shipment" breadcrumbItem="Details" />
          {shipment !== null && shipment.shippingAddress && !isEmpty(shipment) && (
            <Row>
              <Col lg="12">
                <Card>
                  <CardBody>
                    <div className="invoice-title">
                      <h4 className="float-right font-size-16">
                        Order # {shipment.consignmentNumber}
                      </h4>
                      <div className="mb-4">
                        <img src={logo} alt="logo" height="20" />
                      </div>
                    </div>
                    <hr />
                    <Row>
                      <Col xs="6">
                        <address>
                          <strong>Billed To:</strong>
                          <br />
                          <span>{shipment.recipient}</span>

                          <br />
                          {map(
                            shipment.billingAddress &&
                              shipment.billingAddress.split(","),
                            (item, key) => (
                              <React.Fragment key={key}>
                                <span>{item}</span>
                                <br />
                              </React.Fragment>
                            )
                          )}
                        </address>
                      </Col>
                      <Col xs="6" className="text-right">
                        <address>
                          <strong>Shipped To:</strong>
                          <br />
                          <span>{shipment.recipient}</span>
                          <br />
                          {map(
                            shipment.shippingAddress &&
                              shipment.shippingAddress.split(","),
                            (item, key) => (
                              <React.Fragment key={key}>
                                <span>{item}</span>
                                <br />
                              </React.Fragment>
                            )
                          )}
                        </address>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="6" className="mt-3">
                        <address>
                          {/* <strong>Payment Method:</strong>
                          <br />
                          {shipment.card}
                          <br /> */}
                          {shipment.email}
                        </address>
                      </Col>
                      <Col xs="6" className="mt-3 text-right">
                        <address>
                          <strong>Shipping Date:</strong>
                          <br />
                          {shipment.shippingDateTime}
                          <br />
                          <br />
                        </address>
                      </Col>
                    </Row>
                    <div className="py-2 mt-3">
                      <h3 className="font-size-15 font-weight-bold">
                        Order summary
                      </h3>
                    </div>
                    <div className="table-responsive">
                      <Table className="table-nowrap">
                        <thead>
                          <tr>
                            <th style={{ width: "70px" }}>No.</th>
                            <th>Item</th>
                            <th className="text-right">Price</th>
                          </tr>
                        </thead>
                        <tbody>
                          {map(shipment.orderSummary.items, (item, key) => (
                            <tr key={key}>
                              <td>{item.id}</td>
                              <td>{item.item}</td>
                              <td className="text-right">${item.price}</td>
                            </tr>
                          ))}
                          <tr>
                            <td colSpan="2" className="text-right">
                              Sub Total
                            </td>
                            <td className="text-right">
                              ${shipment.orderSummary.subTotal}
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2" className="border-0 text-right">
                              <strong>Shipping</strong>
                            </td>
                            <td className="border-0 text-right">
                              ${shipment.orderSummary.shipping}
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2" className="border-0 text-right">
                              <strong>Duties</strong>
                            </td>
                            <td className="border-0 text-right">
                              ${shipment.orderSummary.duties || 0}
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2" className="border-0 text-right">
                              <strong>Total</strong>
                            </td>
                            <td className="border-0 text-right">
                              <h4 className="m-0">
                                ${shipment.orderSummary.total}
                              </h4>
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                    <div className="py-2 mt-3">
                      <h3 className="font-size-15 font-weight-bold">
                        Order Remarks
                      </h3>
                    </div>
                    <div className="table-responsive">
                      <Table className="table-nowrap">
                        <thead>
                          <tr>
                            <th>Intermediary</th>
                            <th>Remark</th>
                          </tr>
                        </thead>
                        <tbody>
                          {map(shipment.remarks, (item, key) => (
                            <tr key={key}>
                              <td>{item.intermediary}</td>
                              <td>{item.remark}</td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </div>
                    <div className="d-print-none">
                      <div className="float-right">
                        <Link
                          to="#"
                          onClick={printInvoice}
                          className="btn btn-success waves-effect waves-light mr-2"
                        >
                          <i className="fa fa-print" />
                        </Link>
                        <Link
                          to="#"
                          className="btn btn-primary w-md waves-effect waves-light"
                        >
                          Send
                        </Link>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </React.Fragment>
  )
}
export default ShipmentDetails
