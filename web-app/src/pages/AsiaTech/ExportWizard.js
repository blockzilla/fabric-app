import React, { useState, useEffect, useRef } from "react"

import {
  Card,
  CardBody,
  Col,
  Container,
  Button,
  FormGroup,
  Input,
  Label,
  NavItem,
  NavLink,
  Progress,
  Row,
  TabContent,
  TabPane,
} from "reactstrap"
import { useHistory } from "react-router-dom"
import { AvForm, AvField } from "availity-reactstrap-validation"

import classnames from "classnames"
import { Link } from "react-router-dom"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

const ExportWizard = () => {
  const [shipment, setShipment] = useState("true")
  const [activeTabProgress, setactiveTabProgress] = useState(1)
  const [progressValue, setprogressValue] = useState(25)
  const history = useHistory()
  const ws = useRef(null)

  useEffect(() => {
    let storageVal = JSON.parse(localStorage.getItem("shipment"))

    if (storageVal === null || storageVal.recipient === undefined) {
      history.push("/create-shipment")
    } else {
      setShipment(storageVal)
      if (storageVal.currentState <= 4) {
        toggleTabProgress(storageVal.currentState)
      } else if (storageVal.currentState > 4) {
        toggleTabProgress(4)
      }
    }
    connect()
  }, [])

  function connect() {
    ws.current = new WebSocket(`ws://0.0.0.0:8080`)

    ws.current.onerror = function (err) {
      console.error("Socket encountered error: ", err.message, "Closing socket")
      ws.current.close()
    }
    ws.current.onopen = function () {
      console.log("WebSocket connection established")
    }
    ws.current.onclose = function (e) {
      console.log(
        "Socket is closed. Reconnect will be attempted in 1 second.",
        e.reason
      )
      setTimeout(function () {
        connect()
      }, 1000)
    }

    ws.current.onmessage = function (event) {
      const data = JSON.parse(event.data)
      const payload = data.payload

      if (data.type === "processCargoSuccess") {
        handlePayload(payload)
      } else if (data.type === "processCargoFailed") {
        console.error(data.payload)
      } else if (data.type === "exportCustomsSuccess") {
        handlePayload(payload)
      } else if (data.type === "exportCustomsFailed") {
        console.error(data.payload)
      }
    }
  }

  function handlePayload(payload) {
    console.info(payload)
    localStorage.removeItem("shipment")
    localStorage.setItem("shipment", JSON.stringify(payload))
    setShipment(payload)
    if (payload.currentState <= 4) {
      toggleTabProgress(payload.currentState + 1)
    }
  }

  function toggleTabProgress(tab) {
    if (activeTabProgress !== tab) {
      if (tab >= 1 && tab <= 4) {
        setactiveTabProgress(tab)

        if (tab === 1) {
          setprogressValue(25)
        }
        if (tab === 2) {
          setprogressValue(50)
        }
        if (tab === 3) {
          setprogressValue(75)
        }
        if (tab === 4) {
          setprogressValue(100)
        }
      }
    }
  }

  async function processCargo(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("processCargo", values)
    } else {
      console.error(errors)
    }
  }

  async function exportCustoms(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      sendEvent("exportCustoms", values)
    } else {
      console.error(errors)
    }
  }

  async function sendEvent(type, values) {
    var json = {
      type: type,
      payload: {
        recipient: shipment.recipient,
        consignmentNumber: shipment.consignmentNumber,
        remark: values.remark,
        intermediary: values.intermediary,
      },
    }
    ws.current.send(JSON.stringify(json))
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Breadcrumbs title="Export" breadcrumbItem="Export Wizard" />
          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <div id="progrss-wizard" className="twitter-bs-wizard">
                    <ul className="twitter-bs-wizard-nav nav nav-pills nav-justified">
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: activeTabProgress === 1,
                          })}
                          // onClick={() => {
                          //   toggleTabProgress(1)
                          // }}
                        >
                          <span className="step-number mr-2">01</span>
                          Transport to port
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: activeTabProgress === 2,
                          })}
                          // onClick={() => {
                          //   toggleTabProgress(2)
                          // }}
                        >
                          <span className="step-number mr-2">02</span>
                          <span>Cargo Processing</span>
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: activeTabProgress === 3,
                          })}
                          // onClick={() => {
                          //   toggleTabProgress(3)
                          // }}
                        >
                          <span className="step-number mr-2">03</span>
                          Export Customs
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: activeTabProgress === 4,
                          })}
                          // onClick={() => {
                          //   toggleTabProgress(4)
                          // }}
                        >
                          <span className="step-number mr-2">04</span>
                          Shipment Dispatched
                        </NavLink>
                      </NavItem>
                    </ul>
                    <div id="bar" className="mt-4">
                      <Progress
                        color="success"
                        striped
                        animated
                        value={progressValue}
                      />
                      <div className="progress-bar bg-success progress-bar-striped progress-bar-animated" />
                    </div>
                    <TabContent
                      activeTab={activeTabProgress}
                      className="twitter-bs-wizard-tab-content"
                    >
                      <TabPane tabId={1}>
                        <div className="row justify-content-center">
                          <Col lg="6">
                            <div className="text-center">
                              <div className="mb-4">
                                <i className="bx bxs-truck text-success display-4" />
                              </div>
                              <div>
                                <h5>
                                  Shipment #{shipment.consignmentNumber} on
                                  route to Port
                                </h5>
                                <p className="text-muted">
                                  {shipment.remarks &&
                                    shipment.remarks[0].remark}
                                </p>
                              </div>
                            </div>
                          </Col>
                        </div>
                      </TabPane>
                      <TabPane tabId={2}>
                        <div className="row justify-content-center">
                          <Col lg="6">
                            <div className="text-center">
                              <div className="mb-4">
                                <i className="bx bxs-cart-download text-success display-4" />
                              </div>
                              <div>
                                <Row className="text-center">
                                  <Col>
                                    <AvForm
                                      className="needs-validation"
                                      onSubmit={processCargo}
                                    >
                                      <FormGroup>
                                        <Label for="basicpill-cardno-input12">
                                          Enter Remark{" "}
                                        </Label>
                                        <AvField
                                          type="text"
                                          name="remark"
                                          className="form-control"
                                          id="basicpill-cardno-input12"
                                        />
                                        <Label for="intermediary">
                                          Enter Intermediary{" "}
                                        </Label>
                                        <AvField
                                          type="text"
                                          name="intermediary"
                                          className="form-control"
                                          id="intermediary"
                                          readOnly={true}
                                          value="Asia Tech"
                                        />
                                        <br />
                                        <Button
                                          color="primary"
                                          className="btn btn-primary waves-effect waves-light"
                                        >
                                          Process
                                        </Button>
                                      </FormGroup>
                                    </AvForm>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                        </div>
                      </TabPane>
                      <TabPane tabId={3}>
                        <div className="row justify-content-center">
                          <Col lg="6">
                            <div className="text-center">
                              <div className="mb-4">
                                <i className="bx bx-no-entry text-failed display-4" />
                              </div>
                              <div>
                                <Row className="text-center">
                                  <Col>
                                    <AvForm
                                      className="needs-validation"
                                      onSubmit={exportCustoms}
                                    >
                                      <FormGroup>
                                        <Label for="basicpill-cardno-input121">
                                          Enter Remark{" "}
                                        </Label>
                                        <AvField
                                          type="text"
                                          name="remark"
                                          className="form-control"
                                          id="basicpill-cardno-input121"
                                        />
                                        <Label for="intermediary">
                                          Enter Intermediary{" "}
                                        </Label>
                                        <AvField
                                          type="text"
                                          name="intermediary"
                                          className="form-control"
                                          id="intermediary"
                                          readOnly={true}
                                          value="Asia Tech"
                                        />
                                        <br />
                                        <Button
                                          color="primary"
                                          className="btn btn-primary waves-effect waves-light"
                                        >
                                          Process
                                        </Button>
                                      </FormGroup>
                                    </AvForm>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                        </div>
                      </TabPane>
                      <TabPane tabId={4}>
                        <div className="row justify-content-center">
                          <Col lg="6">
                            <div className="text-center">
                              <div className="mb-4">
                                <i className="mdi mdi-check-circle-outline text-success display-4" />
                              </div>
                              <div>
                                <h5>Shipment Dispatched</h5>
                                <p className="text-muted">
                                  Consignment dispatched to shipping company
                                </p>
                              </div>
                            </div>
                          </Col>
                        </div>
                      </TabPane>
                    </TabContent>
                    <ul className="pager wizard twitter-bs-wizard-pager-link">
                      <li
                        className={
                          activeTabProgress === 1
                            ? "previous disabled"
                            : "previous"
                        }
                      >
                        {/* <Link
                          to="#"
                          onClick={() => {
                            toggleTabProgress(activeTabProgress - 1)
                          }}
                        >
                          Previous
                        </Link> */}
                      </li>
                      <li
                        className={
                          activeTabProgress === 4 ? "next disabled" : "next"
                        }
                      >
                        <Link
                          to="#"
                          onClick={() => {
                            toggleTabProgress(activeTabProgress + 1)
                          }}
                        >
                          Next
                        </Link>
                      </li>
                    </ul>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default ExportWizard
