import React, { useState, useEffect, useRef } from "react"
import { createGlobalState } from "react-hooks-global-state"
import { useHistory } from "react-router-dom"

import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  FormGroup,
  Label,
  Row,
} from "reactstrap"
import { AvForm, AvField } from "availity-reactstrap-validation"
//Import Date Picker
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

const { useGlobalState } = createGlobalState({ shipment: {} })

const CreateShipment = () => {
  const history = useHistory()

  const [startDate, setstartDate] = useState(new Date())
  const [shipment, setShipment] = useGlobalState("shipment")
  const ws = useRef(null)

  const startDateChange = date => {
    setstartDate(date)
  }
  async function handleSubmit(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)

      var json = {
        type: "issue",
        payload: values,
      }

      ws.current.send(JSON.stringify(json))
    } else {
      console.error(errors)
    }
  }

  useEffect(() => {
    connect()
  }, [])
  function connect() {
    ws.current = new WebSocket(`ws://0.0.0.0:8080`)

    ws.current.onerror = function (err) {
      console.error("Socket encountered error: ", err.message, "Closing socket")
      ws.current.close()
    }
    ws.current.onopen = function () {
      console.log("WebSocket connection established")
    }
    ws.current.onclose = function (e) {
      console.log(
        "Socket is closed. Reconnect will be attempted in 1 second.",
        e.reason
      )
      setTimeout(function () {
        connect()
      }, 1000)
    }

    ws.current.onmessage = function (event) {
      const data = JSON.parse(event.data)

      if (data.type === "issueSuccess") {
        console.info(data.payload)
        localStorage.removeItem("shipment")
        localStorage.setItem("shipment", JSON.stringify(data.payload))
        setShipment(data.payload)
        history.push("/shipment-details")
      } else if (data.type === "issueFailed") {
        console.error(data.payload)
        history.push("/create-shipment")
      }
    }
  }

  function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Shipments" breadcrumbItem="Shipments Create" />

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <CardTitle className="mb-4">Create New Shipment</CardTitle>
                  <AvForm className="needs-validation" onSubmit={handleSubmit}>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="issuer"
                        className="col-form-label col-lg-2"
                      >
                        Issuer
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="issuer"
                          name="issuer"
                          type="text"
                          className="form-control"
                          placeholder="Aisa Tech"
                          readOnly={true}
                          value="Aisa Tech"
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="recipient"
                        className="col-form-label col-lg-2"
                      >
                        Recipient
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="recipient"
                          name="recipient"
                          type="text"
                          className="form-control"
                          placeholder="Enter Recipient Name..."
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a recipient name",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="email"
                        className="col-form-label col-lg-2"
                      >
                        Email
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="email"
                          name="email"
                          type="email"
                          className="form-control"
                          placeholder="Enter Recipient Email..."
                          readOnly={true}
                          value="orders@euroaudiovisual.com"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a recipient email",
                            },
                            pattern: {
                              value: "[^@ \t\r\n]+@[^@ \t\r\n]+.[^@ \t\r\n]+",
                              errorMessage: "Price can only be numbers",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="shippingAddress"
                        className="col-form-label col-lg-2"
                      >
                        Shipping Address
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="shippingAddress"
                          name="shippingAddress"
                          type="text"
                          className="form-control"
                          placeholder="Enter Shipping Address..."
                          readOnly={true}
                          value="123 Fake Street, Fake City, Fake Country"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a shipping address",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="billingAddress"
                        className="col-form-label col-lg-2"
                      >
                        Billing Address
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="billingAddress"
                          name="billingAddress"
                          type="text"
                          className="form-control"
                          placeholder="Enter Billing Address..."
                          readOnly={true}
                          value="123 Fake Street, Fake City, Fake Country"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a billing address",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="number"
                        className="col-form-label col-lg-2"
                      >
                        Consignment Number
                      </Label>
                      <Col lg="10">
                        <AvField
                          id="number"
                          name="number"
                          type="number"
                          className="form-control"
                          placeholder="Enter Consignment Number..."
                          value={randomNumber(10000, 99999)}
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a consignment number",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="description"
                        className="col-form-label col-lg-2"
                      >
                        Description
                      </Label>
                      <Col lg="10">
                        <AvField
                          rows="3"
                          className="form-control"
                          id="description"
                          name="description"
                          placeholder="Enter Shipment Description..."
                          readOnly={true}
                          value='500 x 65" 4K LCD Flat Screen TV'
                          validate={{
                            required: {
                              value: true,
                              errorMessage:
                                "Please enter a shipment description",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup className="mb-4" row>
                      <label
                        htmlFor="shipmentvalue"
                        className="col-form-label col-lg-2"
                      >
                        Value
                      </label>
                      <Col lg="10">
                        <AvField
                          id="shipmentvalue"
                          name="shipmentvalue"
                          type="number"
                          placeholder="Enter Shipment Value..."
                          className="form-control"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a shipment value",
                            },
                            pattern: {
                              value: "^[0-9]+$",
                              errorMessage: "Value can only be numbers",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup className="mb-4" row>
                      <label
                        htmlFor="shippingcost"
                        className="col-form-label col-lg-2"
                      >
                        Shipping Cost
                      </label>
                      <Col lg="10">
                        <AvField
                          id="shippingcost"
                          name="shippingcost"
                          type="number"
                          placeholder="Enter Shipping Cost..."
                          className="form-control"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a shipment cost",
                            },
                            pattern: {
                              value: "^[0-9]+$",
                              errorMessage: "Cost can only be numbers",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup className="mb-4" row>
                      <Label className="col-form-label col-lg-2">
                        Shipment Date
                      </Label>
                      <Col lg="10">
                        <Row>
                          <Col md={6} className="pr-0">
                            <DatePicker
                              className="form-control"
                              selected={startDate}
                              onChange={startDateChange}
                              name="shipmentdate"
                              validate={{
                                required: {
                                  value: true,
                                  errorMessage: "Please enter a start date",
                                },
                              }}
                            />
                          </Col>
                        </Row>
                      </Col>
                    </FormGroup>

                    <FormGroup className="mb-4" row>
                      <Label
                        htmlFor="remarks"
                        className="col-form-label col-lg-2"
                      >
                        Remarks
                      </Label>
                      <Col lg="10">
                        <AvField
                          rows="3"
                          className="form-control"
                          id="remarks"
                          name="remarks"
                          placeholder="Enter Shipment Remarks..."
                          validate={{
                            required: {
                              value: true,
                              errorMessage: "Please enter a shipment remark",
                            },
                          }}
                        />
                      </Col>
                    </FormGroup>

                    <Row className="justify-content-end">
                      <Col lg="10">
                        <Button
                          type="submit"
                          onSubmit={handleSubmit}
                          color="primary"
                        >
                          Create Shipment
                        </Button>
                      </Col>
                    </Row>
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default CreateShipment
