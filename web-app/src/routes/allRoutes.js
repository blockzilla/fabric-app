import React from "react"
import { Redirect } from "react-router-dom"

// Authentication related pages
import Login from "../pages/Authentication/Login"
import Logout from "../pages/Authentication/Logout"
import Register from "../pages/Authentication/Register"
import ForgetPwd from "../pages/Authentication/ForgetPassword"

// Dashboard
import Dashboard from "../pages/Dashboard/index"
import RecieveGoods from "../pages/EuroAudioVisual/RecieveGoods"
import TrackShipment from "../pages/EuroAudioVisual/TrackShipment"
import ShipmentManager from "../pages/ShenzhenShipping/ShipmentManager"
import ShipmentDetails from "../pages/EuroAudioVisual/ShipmentDetails"

import CreateShipment from "../pages/AsiaTech/CreateShipment"
import ExportWizard from "../pages/AsiaTech/ExportWizard"

const userRoutes = [
  { path: "/dashboard", component: Dashboard },
  { path: "/recieve-goods", component: RecieveGoods },
  { path: "/track-shipment", component: TrackShipment },
  { path: "/shipment-manager", component: ShipmentManager },
  { path: "/create-shipment", component: CreateShipment },
  { path: "/shipment-details", component: ShipmentDetails },
  { path: "/export-wizard", component: ExportWizard },
  // this route should be at the end of all other routes
  { path: "/", exact: true, component: () => <Redirect to="/dashboard" /> },
]

const authRoutes = [
  { path: "/logout", component: Logout },
  { path: "/login", component: Login },
  { path: "/forgot-password", component: ForgetPwd },
  { path: "/register", component: Register },
]

export { userRoutes, authRoutes }
