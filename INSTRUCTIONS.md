## SuezShipping

# Install

Install the fabric binaries and docker imeage

```
curl -sSL https://bit.ly/2ysbOFE | bash -s
```

Start commercial paper

```
cd fabric-samples/commercial-papaer
./network-starter.sh
```

Add binaries fabric to path

```
export PATH=~/github/colledge/fabric-samples/bin:$PATH
export FABRIC_CFG_PATH=~/github/colledge/fabric-samples/config/
```

Suggest that you monitor the docker containers by running

```
./organization/magnetocorp/configuration/cli/monitordocker.sh fabric_test
```
