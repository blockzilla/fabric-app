#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0

function _exit(){
    printf "Exiting:%s\n" "$1"
    exit -1
}

# Exit on first error, print all commands.
set -ev
set -o pipefail

# Where am I?
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export FABRIC_CFG_PATH="${DIR}/../config"

cd "${DIR}/../test-network/"




cd "${DIR}/../test-network/addOrg3"
./addOrg3.sh down
cd "${DIR}/../test-network/"


docker kill cli-shenzhen-shipping cli-asia-tech cli-euro-audio-visual logspout || true
./network.sh down

# remove any stopped containers
docker rm $(docker ps -aq)






