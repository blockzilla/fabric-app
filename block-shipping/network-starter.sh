#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0

function _exit(){
    printf "Exiting:%s\n" "$1"
    exit -1
}

# Exit on first error, print all commands.
set -ev
set -o pipefail


rm -rf ./organization/asia-tech/identity
rm -rf ./organization/shenzhen-shipping/identity

# ./network-clean.sh

# Where am I?
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export FABRIC_CFG_PATH="${DIR}/../config"

cd "${DIR}/../test-network/"

docker kill cli-shenzhen-shipping cli-asia-tech cli-euro-audio-visual logspout || true
./network.sh down
# cd "${DIR}/../test-network/addOrg3"
# ./addOrg3.sh down
# cd "${DIR}/../test-network/"
./network.sh up createChannel -ca -s couchdb
cd "${DIR}/../test-network/addOrg3"
./addOrg3.sh up -ca -s couchdb

# Copy the connection profiles so they are in the correct organizations.
cp "${DIR}/../test-network/organizations/peerOrganizations/org1.example.com/connection-org1.yaml" "${DIR}/organization/asia-tech/gateway/"
cp "${DIR}/../test-network/organizations/peerOrganizations/org2.example.com/connection-org2.yaml" "${DIR}/organization/shenzhen-shipping/gateway/"
cp "${DIR}/../test-network/organizations/peerOrganizations/org3.example.com/connection-org3.yaml" "${DIR}/organization/euro-audio-visual/gateway/"

cp "${DIR}/../test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/signcerts/"* "${DIR}/../test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/signcerts/User1@org1.example.com-cert.pem"
cp "${DIR}/../test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/keystore/"* "${DIR}/../test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/keystore/priv_sk"

cp "${DIR}/../test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/signcerts/"* "${DIR}/../test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/signcerts/User1@org2.example.com-cert.pem"
cp "${DIR}/../test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/keystore/"* "${DIR}/../test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/keystore/priv_sk"

cp "${DIR}/../test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/signcerts/"* "${DIR}/../test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/signcerts/User1@org3.example.com-cert.pem"
cp "${DIR}/../test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/keystore/"* "${DIR}/../test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/keystore/priv_sk"


echo Suggest that you monitor the docker containers by running
echo "./organization/asia-tech/configuration/cli/monitordocker.sh fabric_test"



cd ${DIR}
./cop-contract.sh && \
./install-chaincode-asia-tech.sh && \
./install-chaincode-euro-audio-visual.sh && \
./install-chaincode-shenzhen-shipping.sh

# cd ${DIR}/organization/asia-tech/application/ && \
# node enrollUser.js && \
# node issue.js && \
# node carrige-recieved.js && \
# node export-customs.js


# cd ${DIR}/organization/shenzhen-shipping/application/ && \
# node enrollUser.js && \
# node loading.js && \
# node in-transit.js && \
# node unloading.js && \
# node import-customs.js && \
# node import-duties.js && \
# node loading-carrige.js && \
# node unloading-destination.js
