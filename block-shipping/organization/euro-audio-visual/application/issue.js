/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * This application has 6 basic steps:
 * 1. Select an identity from a wallet
 * 2. Connect to network gateway
 * 3. Access PaperNet network
 * 4. Construct request to issue commercial shippingLog
 * 5. Submit transaction
 * 6. Process response
 */

"use strict";

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require("fs");
const yaml = require("js-yaml");
const { Wallets, Gateway } = require("fabric-network");
const ShippingLog = require("../contract/lib/shippingLog.js");

// Main program function
async function main() {
  // A wallet stores a collection of identities for use
  const wallet = await Wallets.newFileSystemWallet(
    "../identity/user/alan/wallet"
  );

  // A gateway defines the peers used to access Fabric networks
  const gateway = new Gateway();

  // Main try/catch block
  try {
    // Specify userName for network access
    // const userName = 'alan.issuer@magnetocorp.com';
    const userName = "alan";

    // Load connection profile; will be used to locate a gateway
    let connectionProfile = yaml.safeLoad(
      fs.readFileSync("../gateway/connection-org3.yaml", "utf8")
    );

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: true, asLocalhost: true },
    };

    // Connect to gateway using application specified parameters
    console.log("Connect to Fabric gateway.");

    await gateway.connect(connectionProfile, connectionOptions);

    // Access PaperNet network
    console.log("Use network channel: mychannel.");

    const network = await gateway.getNetwork("mychannel");

    // Get addressability to commercial shippingLog contract
    console.log("Use org.papernet.commercialpaper smart contract.");

    const contract = await network.getContract("shippingLog");

    // issue commercial shippingLog
    console.log("Submit shippingLog recieve_carrige transaction.");

    const lastModified = new Date();

    const issueResponse = await contract.submitTransaction(
      "shipped",
      "EuroAudioVisual",
      "00001",
      "EuroAudioVisual",
      "Shipment recieved by EuroAudioVisual",
      lastModified.toISOString()
    );

    // process response
    console.log(
      "Process recieve_carrige transaction response." + issueResponse
    );

    let shippingLog = ShippingLog.fromBuffer(issueResponse);

    console.log(
      `${shippingLog.manufacturer} shippingLog : ${shippingLog.consignmentNumber} successfully issued for value ${shippingLog.consignmentValue}`
    );
    console.log("Transaction complete.");
  } catch (error) {
    console.log(`Error processing transaction. ${error}`);
    console.log(error.stack);
  } finally {
    // Disconnect from the gateway
    console.log("Disconnect from Fabric gateway.");
    gateway.disconnect();
  }
}
main()
  .then(() => {
    console.log("Issue program complete.");
  })
  .catch((e) => {
    console.log("Issue program exception.");
    console.log(e);
    console.log(e.stack);
    process.exit(-1);
  });
