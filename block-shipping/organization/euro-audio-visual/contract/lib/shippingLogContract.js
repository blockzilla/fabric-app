"use strict";

// Fabric smart contract classes
const { Contract, Context } = require("fabric-contract-api");

const ShippingLog = require("./shippingLog.js");
const ShippingLogList = require("./shippingLogList.js");
const QueryUtils = require("./queries.js");

/**
 * A custom context provides easy access to list of all shippingLogs
 */
class ShippingLogContext extends Context {
    constructor() {
        super();
        // All shippingLogs are held in a list of shippingLogs
        this.shippingLogList = new ShippingLogList(this);
    }
}

/**
 * Define shippingLog smart contract by extending Fabric Contract class
 *
 */
class ShippingLogContract extends Contract {
    constructor() {
        // Unique namespace when multiple contracts per chaincode file
        super("org.shippingLognet.ShippingLog");
    }

    /**
     * Define a custom context for shippingLog
     */
    createContext() {
        return new ShippingLogContext();
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async instantiate(ctx) {
        // No implementation required with this example
        // It could be where data migration is performed, if necessary
        console.log("Instantiate the contract");
        return "Instantiate the contract";
    }

    /**
     * Issue consignment
     *
     * @param {Context} ctx the transaction context
     * @param {String} manufacturer manufacturer
     * @param {String} recipient recipient for shipment
     * @param {Integer} consignmentNumber consignment number of the shipment
     * @param {String} shippingDateTime shipping date time
     * @param {Integer} shipmentvalue value of shipment
     * @param {Array} remarks comments relating to shipment
     */
    async issue(
        ctx,
        manufacturer,
        recipient,
        consignmentNumber,
        description,
        shippingDateTime,
        shipmentvalue,
        shippingcost,
        remarks,
        email,
        shippingAddress,
        billingAddress
    ) {
        let shippingLogKey = ShippingLog.makeKey([
            recipient,
            consignmentNumber,
        ]);
        let shippingLog = await ctx.shippingLogList.getShippingLog(
            shippingLogKey
        );
        if (shippingLog !== null) {
            throw new Error(
                `Shipment record {1} already exists`,
                consignmentNumber
            );
        }

        var orderSummary = {
            items: [
                {
                    id: 1,
                    item: description,
                    price: shipmentvalue,
                },
            ],
            subTotal: shipmentvalue,
            shipping: shippingcost,
            duties: 0,
            total: parseInt(shippingcost) + parseInt(shipmentvalue),
        };

        let remarksList = [];
        remarksList.push({
            intermediary: manufacturer,
            remark: remarks,
        });

        // create an instance of the shippingLog
        shippingLog = ShippingLog.createInstance(
            manufacturer,
            recipient,
            manufacturer,
            consignmentNumber,
            orderSummary,
            shippingDateTime,
            shippingDateTime,
            "2020",
            shippingcost + shipmentvalue,
            0,
            false,
            remarksList,
            email,
            shippingAddress,
            billingAddress
        );

        // Smart contract, moves shipment to into TRANSIT_TO_PORT state
        shippingLog.setInTransitToPort();

        let mspid = ctx.clientIdentity.getMSPID();
        shippingLog.setOwnerMSP(mspid);

        console.log(shippingLog);

        // Add the shippingLog to the list of all similar shipments in the world state
        await ctx.shippingLogList.addShippingLog(shippingLog);

        // Must return a serialized shipment log to caller of smart contract
        return shippingLog;
    }

    /**
     * Recieve consignment at port
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async recieve_carrige(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isRecievedCarrige()) {
            throw new Error(
                `Shipment {1} is already status carrige recieved`,
                consignmentNumber
            );
        }

        shippingLog.setRecievedCarrige();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Process export customs
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async export_customs(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isExportCustoms()) {
            throw new Error(
                `Shipment {1} is already being processed export customs`,
                consignmentNumber
            );
        }

        shippingLog.setExportCustoms();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Loading consingment on ship for transit
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async loading(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isLoading()) {
            throw new Error(
                `Shipment {1} has already been loaded`,
                consignmentNumber
            );
        }

        shippingLog.setLoading();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Consignment in transit
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async in_transit(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isInTranit()) {
            throw new Error(
                `Shipment {1} is already in transit`,
                consignmentNumber
            );
        }

        shippingLog.setInTranit();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Unload consignment from ship
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async unloading(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isUnloading()) {
            throw new Error(
                `Shipment {1} is already unloaded`,
                consignmentNumber
            );
        }

        shippingLog.setUnloading();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Process import customs checks
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async import_customs(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isImportCustoms()) {
            throw new Error(
                `Shipment {1} import customs have already been processed`,
                consignmentNumber
            );
        }

        shippingLog.setImportCustoms();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Process import duties for final destination
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     * @param {Integer} dutiesValue value of duties to be paid
     */
    async import_duties(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime,
        dutiesValue
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isImportDuties()) {
            throw new Error(
                `Shipment {1} import duties have already been processed`,
                consignmentNumber
            );
        }
        var shippingcost = shippingLog.orderSummary.shipping;
        var shipmentvalue = shippingLog.orderSummary.subTotal;
        var setConsignmentValue =
            parseInt(shippingcost) +
            parseInt(shipmentvalue) +
            parseInt(dutiesValue);

        var orderSummary = shippingLog.orderSummary;
        orderSummary.duties = dutiesValue;
        orderSummary.total = setConsignmentValue;

        shippingLog.setImportDuties();
        shippingLog.setOrderSummary(orderSummary);
        shippingLog.setConsignmentValue(setConsignmentValue);
        shippingLog.setDutiesValue(dutiesValue);
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remarks: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Loading consignment for delievery to final destination
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async loading_carrige(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isLoadingCarrige()) {
            throw new Error(
                `Shipment {1} has already been loaded for delievery`,
                consignmentNumber
            );
        }

        shippingLog.setLoadingCarrige();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Unloading goods at final destination
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} intermediary current entity processing shipment
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async unloading_destination(
        ctx,
        recipient,
        consignmentNumber,
        intermediary,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isUnloadingDestination()) {
            throw new Error(
                `Shipment {1} has aready been unloaded at destination`,
                consignmentNumber
            );
        }

        shippingLog.setUnloadingDestination();
        shippingLog.setIntermediary(intermediary);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    /**
     * Finalise shipping, goods recieved at destination
     *
     * @param {Context} ctx the transaction context
     * @param {String} recipient shipping recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     * @param {String} remarks status updates associated with actions
     * @param {String} lastModifiedDateTime timestamp for last event
     */
    async shipped(
        ctx,
        recipient,
        consignmentNumber,
        remarks,
        lastModifiedDateTime
    ) {
        let shippingLog = await this.checkIsValid(
            ctx,
            recipient,
            consignmentNumber
        );

        if (shippingLog.isShipped()) {
            throw new Error(
                `Shipment {1} has already been marked as shipped`,
                consignmentNumber
            );
        }

        shippingLog.setShipped();
        shippingLog.setIntermediary(recipient);
        shippingLog.setLastModifiedDateTime(lastModifiedDateTime);

        shippingLog.remarks.push({
            intermediary: intermediary,
            remark: remarks,
        });
        console.log(shippingLog);

        // Update the shippingLog
        await ctx.shippingLogList.updateShippingLog(shippingLog);
        return shippingLog;
    }

    // Query transactions

    /**
     * Query history of a shippingLog
     * @param {Context} ctx the transaction context
     * @param {String} recipient shippingLog recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     */
    async queryHistory(ctx, recipient, consignmentNumber) {
        // Get a key to be used for History query

        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let results = await query.getAssetHistory(recipient, consignmentNumber); // (cpKey);
        return results;
    }
    // Query transactions

    /**
     * Query history of a shippingLog
     * @param {Context} ctx the transaction context
     * @param {String} recipient shippingLog recipient
     * @param {Integer} consignmentNumber consignment number for this manufacturer
     */
    async getAllResults(ctx) {
        // Get a key to be used for History query

        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let results = await query.getAllResults(); // (cpKey);
        return results;
    }

    /**
     * queryOwner shippingLog: supply name of owning org, to find list of shippingLogs based on owner field
     * @param {Context} ctx the transaction context
     * @param {String} owner shippingLog owner
     */
    async queryOwner(ctx, owner) {
        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let owner_results = await query.queryKeyByOwner(owner);

        return owner_results;
    }

    /**
     * queryPartial shippingLog - provide a prefix eg. "DigiBank" will list all shippingLogs _issued_ by DigiBank etc etc
     * @param {Context} ctx the transaction context
     * @param {String} prefix asset class prefix (added to shippingLoglist namespace) eg. org.shippingLognet.shippingLogMagnetoCorp asset listing: shippingLogs issued by MagnetoCorp.
     */
    async queryPartial(ctx, prefix) {
        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let partial_results = await query.queryKeyByPartial(prefix);

        return partial_results;
    }

    /**
     * queryAdHoc shippingLog - supply a custom mango query
     * eg - as supplied as a param:
     * ex1:  ["{\"selector\":{\"faceValue\":{\"$lt\":8000000}}}"]
     * ex2:  ["{\"selector\":{\"faceValue\":{\"$gt\":4999999}}}"]
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString querystring
     */
    async queryAdhoc(ctx, queryString) {
        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let querySelector = JSON.parse(queryString);
        let adhoc_results = await query.queryByAdhoc(querySelector);

        return adhoc_results;
    }

    /**
     * queryNamed - supply named query - 'case' statement chooses selector to build (pre-canned for demo purposes)
     * @param {Context} ctx the transaction context
     * @param {String} queryname the 'named' query (built here) - or - the adHoc query string, provided as a parameter
     */
    async queryNamed(ctx, queryname) {
        let querySelector = {};
        switch (queryname) {
            case "redeemed":
                querySelector = { selector: { currentState: 4 } }; // 4 = redeemd state
                break;
            case "trading":
                querySelector = { selector: { currentState: 3 } }; // 3 = trading state
                break;
            case "value":
                // may change to provide as a param - fixed value for now in this sample
                querySelector = { selector: { faceValue: { $gt: 4000000 } } }; // to test, issue CommPapers with faceValue <= or => this figure.
                break;
            default:
                // else, unknown named query
                throw new Error(
                    "invalid named query supplied: " +
                        queryname +
                        "- please try again "
                );
        }

        let query = new QueryUtils(ctx, "org.shippingLognet.shippingLog");
        let adhoc_results = await query.queryByAdhoc(querySelector);

        return adhoc_results;
    }

    async checkIsValid(ctx, recipient, consignmentNumber) {
        // Retrieve the current shippingLog using key fields provided
        let shippingLogKey = ShippingLog.makeKey([
            recipient,
            consignmentNumber,
        ]);
        let shippingLog = await ctx.shippingLogList.getShippingLog(
            shippingLogKey
        );
        if (shippingLog === null) {
            throw new Error(
                "No item with consignment number found: " + consignmentNumber
            );
        }
        return shippingLog;
    }
}

module.exports = ShippingLogContract;
