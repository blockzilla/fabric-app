"use strict";

// Utility class for ledger state
const State = require("../ledger-api/state.js");

// Enumerate commercial shippingLog state values
const slState = {
    TRANSIT_TO_PORT: 1, // manufacturer
    RECIEVE_CARRIGE: 2, // port 1
    EXPORT_CUSTOMS: 3, // port 1
    LOADING: 4, // shipping
    IN_TRANSIT: 5, // shipping
    UNLOADING: 6, // shipping
    IMPORT_CUSTOMS: 7, // port 2
    IMPORT_DUTIES: 8, // port 2
    LOADING_CARRIGE: 9, // ground transport
    UNLOADING_DESTINATION: 10, // ground transport
    SHIPPED: 11, // recipient
};

/**
 * ShippingLog class extends State class
 * Class will be used by application and smart contract to define a shippingLog
 */
class ShippingLog extends State {
    constructor(obj) {
        super(ShippingLog.getClass(), [obj.recipient, obj.consignmentNumber]);
        Object.assign(this, obj);
    }

    /**
     * Basic getters and setters
     */
    getManufacturer() {
        return this.manufacturer;
    }

    setManufacturer(newManufacturer) {
        this.manufacturer = newManufacturer;
    }

    getRecipient() {
        return this.recipient;
    }

    setRecipient(newRecipient) {
        this.recipient = newRecipient;
    }

    getIntermediary() {
        return this.intermediary;
    }

    setIntermediary(newIntermediary) {
        this.intermediary = newIntermediary;
    }

    getLastModifiedDateTime() {
        return this.lastModified;
    }

    setLastModifiedDateTime(newLastModified) {
        this.lastModifiedDateTime = newLastModified;
    }

    setDutiesValue(newDutiesValue) {
        this.dutiesValue = newDutiesValue;
    }

    setOrderSummary(newOrderSummary) {
        this.orderSummary = newOrderSummary;
    }

    setConsignmentValue(newConsignmentValue) {
        this.consignmentValue = newConsignmentValue;
    }

    setOwnerMSP(mspid) {
        this.mspid = mspid;
    }

    getOwnerMSP() {
        return this.mspid;
    }

    setOwner(newOwner) {
        this.owner = newOwner;
    }

    /**
     * Useful methods to encapsulate commercial shippingLog states
     */
    setInTransitToPort() {
        this.currentState = slState.TRANSIT_TO_PORT;
    }

    setRecievedCarrige() {
        this.currentState = slState.RECIEVE_CARRIGE;
    }

    setExportCustoms() {
        this.currentState = slState.EXPORT_CUSTOMS;
    }

    setInTranit() {
        this.currentState = slState.IN_TRANSIT;
    }

    setLoading() {
        this.currentState = slState.LOADING;
    }

    setUnloading() {
        this.currentState = slState.UNLOADING;
    }

    setImportCustoms() {
        this.currentState = slState.IMPORT_CUSTOMS;
    }

    setImportDuties() {
        this.currentState = slState.IMPORT_DUTIES;
    }

    setLoadingCarrige() {
        this.currentState = slState.LOADING_CARRIGE;
    }

    setUnloadingDestination() {
        this.currentState = slState.UNLOADING_DESTINATION;
    }

    setShipped() {
        this.currentState = slState.SHIPPED;
    }

    isInTransitToPort() {
        return this.currentState == slState.TRANSIT_TO_PORT;
    }

    isRecievedCarrige() {
        return this.currentState == slState.RECIEVE_CARRIGE;
    }

    isExportCustoms() {
        return this.currentState == slState.EXPORT_CUSTOMS;
    }

    isInTranit() {
        return this.currentState == slState.IN_TRANSIT;
    }

    isUnloading() {
        return this.currentState == slState.UNLOADING;
    }

    isLoading() {
        return this.currentState == slState.LOADING;
    }

    isImportCustoms() {
        return this.currentState == slState.IMPORT_CUSTOMS;
    }

    isImportDuties() {
        return this.currentState == slState.IMPORT_DUTIES;
    }

    isLoadingCarrige() {
        return this.currentState == slState.LOADING_CARRIGE;
    }

    isUnloadingDestination() {
        return this.currentState == slState.UNLOADING_DESTINATION;
    }

    isUnloadingDestination() {
        return this.currentState == slState.UNLOADING_DESTINATION;
    }

    static fromBuffer(buffer) {
        return ShippingLog.deserialize(buffer);
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this));
    }

    /**
     * Deserialize a state data to commercial shippingLog
     * @param {Buffer} data to form back into the object
     */
    static deserialize(data) {
        return State.deserializeClass(data, ShippingLog);
    }

    /**
     * Factory method to create a commercial shippingLog object
     */
    static createInstance(
        manufacturer,
        recipient,
        intermediary,
        consignmentNumber,
        orderSummary,
        shippingDateTime,
        lastModifiedDateTime,
        arrivalDateTime,
        consignmentValue,
        dutiesValue,
        customsStatus,
        remarks,
        email,
        shippingAddress,
        billingAddress
    ) {
        return new ShippingLog({
            manufacturer,
            recipient,
            intermediary,
            consignmentNumber,
            orderSummary,
            shippingDateTime,
            lastModifiedDateTime,
            arrivalDateTime,
            consignmentValue,
            dutiesValue,
            customsStatus,
            remarks,
            email,
            shippingAddress,
            billingAddress,
        });
    }

    static getClass() {
        return "org.papernet.ShippingLog";
    }
}

module.exports = ShippingLog;
