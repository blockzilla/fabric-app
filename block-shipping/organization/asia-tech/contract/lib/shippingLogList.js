"use strict";

// Utility class for collections of ledger states --  a state list
const StateList = require("../ledger-api/statelist.js");

const ShippingLog = require("./shippingLog.js");

class ShippingLogList extends StateList {
    constructor(ctx) {
        super(ctx, "org.shippingLognet.shippingLog");
        this.use(ShippingLog);
    }

    async addShippingLog(shippingLog) {
        return this.addState(shippingLog);
    }

    async getShippingLog(shippingLogKey) {
        return this.getState(shippingLogKey);
    }

    async updateShippingLog(shippingLog) {
        return this.updateState(shippingLog);
    }
}

module.exports = ShippingLogList;
