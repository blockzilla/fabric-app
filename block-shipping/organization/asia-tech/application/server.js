const WebSocket = require("ws");
const enrollUser = require("./enrollUser");
const txManager = require("./txManager");
var Logger = require("bunyan");
var log = new Logger({ name: "txManager" });
const name = "billy";
// enrole the user detials so we can later call the smart contract
enrollUser.enroll(name);

const wss = new WebSocket.Server({ port: 8080 });
let ws;

const statuses = [
  {
    id: 1,
    intermediary: "Ordered",
    iconClass: "bx-copy-alt",
    remarks: "New common language will be more simple than existing.",
  },
  {
    id: 2,
    intermediary: "Packed",
    iconClass: "bx-package",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 3,
    intermediary: "Shipped",
    iconClass: "bx-car",
    remarks: "It will be as simple as occidental in fact it will be Cambridge",
  },
  {
    id: 4,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 5,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 6,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 7,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 8,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 9,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 10,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "To an English person, it will seem like simplified English existence.",
  },
  {
    id: 11,
    intermediary: "Delivered",
    iconClass: "bx-badge-check",
    remarks:
      "Bill an English person, it will seem like simplified English existence.",
  },
];

// handle data requests from client app
wss.on("connection", function connection(socket) {
  ws = socket;
  ws.on("message", function incoming(message) {
    let msg = JSON.parse(message);
    if (msg.type === "issue") {
      handleIssue(msg);
    } else if (msg.type === "processCargo") {
      handleProcessCargo(msg);
    } else if (msg.type === "exportCustoms") {
      handleExportCustoms(msg);
    } else {
      log.info("received: %s", message);
    }
  });
});

// reply to client app
function sendPayload(type, payload) {
  var msg = {
    type: type,
    payload: payload,
  };
  ws.send(JSON.stringify(msg));
}

// handle issuing a shipment and reply
async function handleIssue(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "issue",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 1
  ) {
    sendPayload("issueSuccess", issueRsponse);
  } else {
    sendPayload("issueFailed", {});
  }
}

// handle process cargo transaciton
async function handleProcessCargo(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "processCargo",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 2
  ) {
    sendPayload("processCargoSuccess", issueRsponse);
  } else {
    sendPayload("processCargoFailed", {});
  }
}

// handle export cargo transaciton
async function handleExportCustoms(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "exportCustoms",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 3
  ) {
    sendPayload("exportCustomsSuccess", issueRsponse);
  } else {
    sendPayload("exportCustomsFailed", {});
  }
}
