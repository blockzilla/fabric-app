/*

Shipping tx manager for the manufacturer shipping process

Factory -> Customs

*/

"use strict";

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require("fs");
const yaml = require("js-yaml");
const { Wallets, Gateway } = require("fabric-network");
const ShippingLog = require("../contract/lib/shippingLog.js");
var Logger = require("bunyan");
var log = new Logger({ name: "txManager" });

async function processTransactions(name, actionType, consignment) {
  const wallet = await Wallets.newFileSystemWallet(
    "../identity/user/" + name + "/wallet"
  );

  // A gateway defines the peers used to access Fabric networks
  const gateway = new Gateway();
  let shippingLog = null;

  try {
    // Load connection profile; will be used to locate a gateway
    let connectionProfile = yaml.safeLoad(
      fs.readFileSync("../gateway/connection-org1.yaml", "utf8")
    );

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: name,
      wallet: wallet,
      discovery: { enabled: true, asLocalhost: true },
    };

    // Connect to gateway using application specified parameters
    log.info("Connect to Fabric gateway. .....");

    await gateway.connect(connectionProfile, connectionOptions);

    log.info("Use network channel: mychannel.");

    const network = await gateway.getNetwork("mychannel");
    const contract = await network.getContract("shippingLog");

    log.info("Submit shippingLog issue transaction.");

    //
    // Handlde the different actions that can happen on a shipment
    //
    switch (actionType) {
      case "issue":
        shippingLog = ShippingLog.fromBuffer(
          await issueShipment(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.number} successfully issued for delievery to ${consignment.recipient}`
        );
        break;
      case "processCargo":
        shippingLog = ShippingLog.fromBuffer(
          await processCargo(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} successfully cargo recieved at port`
        );
        break;
      case "exportCustoms":
        shippingLog = ShippingLog.fromBuffer(
          await exportCustoms(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} successfully processed export customs`
        );
        break;

      default:
    }
    log.info("Transaction complete.");
  } catch (error) {
    log.error(`Error processing transaction. ${error}`);
    log.error(error.stack);
  } finally {
    log.info("Disconnect from Fabric gateway.");
    gateway.disconnect();
  }

  if (shippingLog !== null) {
    return shippingLog;
  } else {
    return {};
  }
}

// create a shipment
async function issueShipment(contract, consignment) {
  const issuedDateTime = new Date();
  const issueResponse = await contract.submitTransaction(
    "issue",
    consignment.issuer,
    consignment.recipient,
    consignment.number,
    consignment.description,
    issuedDateTime.toISOString(),
    consignment.shipmentvalue,
    consignment.shippingcost,
    consignment.remarks,
    consignment.email,
    consignment.shippingAddress,
    consignment.billingAddress
  );
  log.info("Process issue shipment transaction: " + issueResponse);

  return issueResponse;
}

// process the cargo accepted stage, i.e. when cargo reaches port
async function processCargo(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "recieve_carrige",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process cargo recieved transaction: " + issueResponse);
  return issueResponse;
}

// process the export customs
async function exportCustoms(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "export_customs",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process export customs transaction: " + issueResponse);
  return issueResponse;
}

module.exports.processTransactions = processTransactions;
