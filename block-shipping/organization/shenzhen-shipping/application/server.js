const WebSocket = require("ws");
const enrollUser = require("./enrollUser");
const txManager = require("./txManager");
var Logger = require("bunyan");
var log = new Logger({ name: "txManager" });
const name = "jim";
// enrole the user detials so we can later call the smart contract
enrollUser.enroll(name);

const wss = new WebSocket.Server({ port: 8081 });
let ws;

// handle data requests from client app
wss.on("connection", function connection(socket) {
  ws = socket;
  ws.on("message", function incoming(message) {
    let msg = JSON.parse(message);

    switch (msg.type) {
      case "loading":
        handleMessage(msg);
        break;
      case "in_transit":
        handleMessage(msg);
        break;
      case "unloading":
        handleMessage(msg);
        break;
      case "import_customs":
        handleMessage(msg);
        break;
      case "import_duties":
        handleMessage(msg);
        break;
      case "loading_carrige":
        handleMessage(msg);
        break;
      case "unloading_destination":
        handleMessage(msg);
        break;
      default:
        log.info("received: %s", message);
    }
  });
});

// reply to client app
function sendPayload(type, payload) {
  var msg = {
    type: type,
    payload: payload,
  };
  ws.send(JSON.stringify(msg));
}

// handle loading cargo transaciton
async function handleMessage(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    msg.type,
    msg.payload
  );
  if (issueRsponse.currentState !== undefined) {
    sendPayload("success", issueRsponse);
  } else {
    sendPayload("failure", {});
  }
}

// handle loading cargo transaciton
async function handleLoading(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "loading",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 4
  ) {
    sendPayload("loadingSuccess", issueRsponse);
  } else {
    sendPayload("loadingFailed", {});
  }
}

// handle in stansit cargo transaciton
async function handleInTransit(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "in_transit",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 5
  ) {
    sendPayload("in_transitSuccess", issueRsponse);
  } else {
    sendPayload("in_transitFailed", {});
  }
}

// handle in handleUnloading cargo transaciton
async function handleUnloading(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "unloading",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 6
  ) {
    sendPayload("unloadingSuccess", issueRsponse);
  } else {
    sendPayload("unloadingFailed", {});
  }
}

// handle in handleImportCustoms cargo transaciton
async function handleImportCustoms(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "import_customs",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 7
  ) {
    sendPayload("import_customsSuccess", issueRsponse);
  } else {
    sendPayload("import_customsFailed", {});
  }
}

// handle in handleImpoerDuties cargo transaciton
async function handleImpoerDuties(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "import_duties",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 8
  ) {
    sendPayload("import_dutiesSuccess", issueRsponse);
  } else {
    sendPayload("import_dutiesFailed", {});
  }
}

// handle in handleLoadingCarrige cargo transaciton
async function handleLoadingCarrige(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "loading_carrige",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 9
  ) {
    sendPayload("loading_carrigeSuccess", issueRsponse);
  } else {
    sendPayload("loading_carrigeFailed", {});
  }
}

// handle in handleUnloadingDestination cargo transaciton
async function handleUnloadingDestination(msg) {
  var issueRsponse = await txManager.processTransactions(
    name,
    "unloading_destination",
    msg.payload
  );
  if (
    issueRsponse.currentState !== undefined &&
    issueRsponse.currentState === 10
  ) {
    sendPayload("unloading_destinationSuccess", issueRsponse);
  } else {
    sendPayload("unloading_destinationFailed", {});
  }
}
