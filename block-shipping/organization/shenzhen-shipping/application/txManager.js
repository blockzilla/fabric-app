/*

Shipping tx manager for the manufacturer shipping process

Factory -> Customs

*/

"use strict";

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require("fs");
const yaml = require("js-yaml");
const { Wallets, Gateway } = require("fabric-network");
const ShippingLog = require("../contract/lib/shippingLog.js");
var Logger = require("bunyan");
var log = new Logger({ name: "txManager" });

async function processTransactions(name, actionType, consignment) {
  const wallet = await Wallets.newFileSystemWallet(
    "../identity/user/" + name + "/wallet"
  );

  // A gateway defines the peers used to access Fabric networks
  const gateway = new Gateway();
  let shippingLog = null;

  try {
    // Load connection profile; will be used to locate a gateway
    let connectionProfile = yaml.safeLoad(
      fs.readFileSync("../gateway/connection-org2.yaml", "utf8")
    );

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: name,
      wallet: wallet,
      discovery: { enabled: true, asLocalhost: true },
    };

    // Connect to gateway using application specified parameters
    log.info("Connect to Fabric gateway. .....");

    await gateway.connect(connectionProfile, connectionOptions);

    log.info("Use network channel: mychannel.");

    const network = await gateway.getNetwork("mychannel");
    const contract = await network.getContract("shippingLog");

    log.info("Submit shippingLog issue transaction.");

    //
    // Handlde the different actions that can happen on a shipment
    //
    switch (actionType) {
      case "loading":
        shippingLog = ShippingLog.fromBuffer(
          await loading(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.number} successfully loads onto ship`
        );
        break;
      case "in_transit":
        shippingLog = ShippingLog.fromBuffer(
          await in_transit(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} cargo in transit`
        );
        break;
      case "unloading":
        shippingLog = ShippingLog.fromBuffer(
          await unloading(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} cargo successfully unloaded from ship`
        );
        break;
      case "import_customs":
        shippingLog = ShippingLog.fromBuffer(
          await import_customs(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} processing import customs`
        );
        break;
      case "import_duties":
        shippingLog = ShippingLog.fromBuffer(
          await import_duties(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} processing import duties`
        );
        break;
      case "loading_carrige":
        shippingLog = ShippingLog.fromBuffer(
          await loading_carrige(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} processing loading carrige`
        );
        break;
      case "unloading_destination":
        shippingLog = ShippingLog.fromBuffer(
          await unloading_destination(contract, consignment)
        );
        log.info(
          `Shipment number: #${consignment.consignmentNumber} processing unloading destination`
        );
        break;

      default:
    }
    log.info("Transaction complete.");
  } catch (error) {
    log.error(`Error processing transaction. ${error}`);
    log.error(error.stack);
  } finally {
    log.info("Disconnect from Fabric gateway.");
    gateway.disconnect();
  }

  if (shippingLog !== null) {
    return shippingLog;
  } else {
    return {};
  }
}

// process the cargo loading onto ship
async function loading(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "loading",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process cargo loading transaction: " + issueResponse);
  return issueResponse;
}

// process the in_transit transaction
async function in_transit(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "in_transit",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process in_transit transaction: " + issueResponse);
  return issueResponse;
}

// process the unloading transaction
async function unloading(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "unloading",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process in_transit transaction: " + issueResponse);
  return issueResponse;
}

// process the import_customs transaction
async function import_customs(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "import_customs",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process import_customs transaction: " + issueResponse);
  return issueResponse;
}

// process the import_duties transaction
async function import_duties(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "import_duties",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString(),
    consignment.duties
  );
  log.info("Process import_duties transaction: " + issueResponse);
  return issueResponse;
}

// process the loading_carrige transaction
async function loading_carrige(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "loading_carrige",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process loading_carrige transaction: " + issueResponse);
  return issueResponse;
}

// process the unloading_destination transaction
async function unloading_destination(contract, consignment) {
  const lastModified = new Date();
  const issueResponse = await contract.submitTransaction(
    "unloading_destination",
    consignment.recipient,
    consignment.consignmentNumber,
    consignment.intermediary,
    consignment.remark,
    lastModified.toISOString()
  );
  log.info("Process unloading_destination transaction: " + issueResponse);
  return issueResponse;
}

module.exports.processTransactions = processTransactions;
