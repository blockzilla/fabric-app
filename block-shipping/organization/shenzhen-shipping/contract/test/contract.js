/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
"use strict";

const ShippingLogContract = require("../lib/shippingLogContract");
const { Stub } = require("fabric-shim");
const { Context } = require("fabric-contract-api");

const chai = require("chai");

require("chai").should();
const sinon = require("sinon");

const sinonChai = require("sinon-chai");
const expect = chai.expect;
describe("Chaincode", () => {
    describe("#Init", () => {
        it("should work", async () => {
            const cc = new ShippingLogContract();
            const stub = sinon.createStubInstance(Stub);

            const res = await cc.instantiate(stub);

            expect(res).to.equal("Instantiate the contract");
        });
    });

    describe("#Invoke shipment workflows", async () => {
        it("should issue a shipment", async () => {
            const issuedDateTime = new Date();

            let transactionContext = new Context();
            const stub = sinon.createStubInstance(Stub);
            transactionContext.setChaincodeStub(stub);

            const cc = new ShippingLogContract();

            const res = await cc.instantiate(transactionContext);
            let ctx = cc.createContext();

            try {
                await cc.issue(
                    ctx,
                    "AsiaTech",
                    "EuroAudioVisual",
                    "AsiaTech",
                    "00001",
                    issuedDateTime.toISOString(),
                    issuedDateTime.toISOString(),
                    "30000",
                    "Item Leaving "
                );

                expect(true).to.eql(true);
            } catch (err) {
                expect(err.message).to.equal("The consignment issue failed");
            }
        });

        it("should issue a shipment", async () => {
            const issuedDateTime = new Date();

            let transactionContext = new Context();
            const stub = sinon.createStubInstance(Stub);
            transactionContext.setChaincodeStub(stub);

            const cc = new ShippingLogContract();

            const res = await cc.instantiate(transactionContext);
            let ctx = cc.createContext();
            try {
                await cc.issue(
                    ctx,
                    "AsiaTech",
                    "00001",
                    "Shenzhen Port",
                    "Items arrived at Shenzhen Port",
                    issuedDateTime.toISOString()
                );

                expect(true).to.eql(true);
            } catch (err) {
                expect(err.message).to.equal(
                    "The recieve_carrige event issue failed"
                );
            }
        });

        it("should issue a shipment", async () => {
            const issuedDateTime = new Date();

            let transactionContext = new Context();
            const stub = sinon.createStubInstance(Stub);
            transactionContext.setChaincodeStub(stub);

            const cc = new ShippingLogContract();

            const res = await cc.instantiate(transactionContext);
            let ctx = cc.createContext();
            try {
                await cc.issue(
                    ctx,
                    "recieve_carrige",
                    "AsiaTech",
                    "00001",
                    "Shenzhen Port",
                    "Items arrived at Shenzhen Port",
                    issuedDateTime.toISOString()
                );

                expect(true).to.eql(true);
            } catch (err) {
                expect(err.message).to.equal(
                    "The recieve_carrige event issue failed"
                );
            }
        });
    });
});
