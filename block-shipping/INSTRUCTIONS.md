# Package, install and approve the smart contract as MagnetoCorp

1.         Make sure the network is up running, and a channel is created.

2.         Navigate to the MagnetoCorp directory:

> $ cd commercial-paper/organization/magnetocorp/

3.         Run the following command to create the chaincode package:

> $ peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0

Before installing the chaincode on the MagnetoCorp peer, you first need to set the environment variable to operate the peer CLI as the MagnetoCorp admit user. A script is provided by the fabric sample to set the environment variables in your command window.

4.         Run the following command in the ***magnetocorp*** directory:

> $ source magnetocorp.sh

You will see the full list of environment variables printed in your window.

The MagnetoCorp admin can now install the chaincode on the MagnetoCorp peer.

5.         Use ***the peer lifecycle chaincode install*** command to install the chaincode on MagnetoCorp peer:

> $ peer lifecycle chaincode install cp.tar.gz

Once the chaincode is installed, we need to approve the chaincode package definition for **_papercontract_** as MagnetoCorp. First, we need to find the package ID of the chaincode we just installed on our peer.

6.         Query the package ID using the peer lifecycle ***chaincode queryinstalled*** command:

> $ peer lifecycle chaincode queryinstalled

7.         Save the returned package ID as an environment variable:

> $ export PACKAGE_ID=cp_0:ffda93e26b183e231b7e9d5051e1ee7ca47fbf24f00a8376ec54120b1a2a335c

The chaincode definition can now be approved for MagnetoCorp.

8.         Use the following command to approve the chaincode definition:

> $ peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name shippingLog -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA

Now we need to do the same for the second organisation; DigiBank.

#Package, install and approve the smart contract as DigiBank

1.         Open a new terminal window in the ***fabric-samples*** repository and navigate to the folder that contains the DigiBank smart contract and application files:

> $ cd commercial-paper/organization/digibank/

2.         Package the ***shippingLog*** smart contract:

> $ peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0

3.         Run the script in the DigiBank folder to set the environment variables that will allow you to act as the DigiBank admin:

> $ source digibank.sh

4.         Install the chaincode on the DigiBank peer:

> $ peer lifecycle chaincode install cp.tar.gz

5.         Query the package ID of the chaincode that was just installed:

> $ peer lifecycle chaincode queryinstalled

6.         Save the returned package ID as an environment variable.

> $ export PACKAGE_ID=cp_0:61b9d0e7ee57237ed0dfa8e4f6a7bd574d21aa75feb435e152c7ae1122072c7c

7.         Approve the chaincode definition of ***shippingLog.***

> $ peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name shippingLog -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA

#Commit the chaincode definition to the channel

Now that DigiBank and MagnetoCorp have both approved the PaperNet chaincode and since either organisation can commit the chaincode to the channel, we will continue operating as the DigiBank admin:

1.         Make sure you are still in the DigiBank directory and use the command below to commit the chaincode definition of ***shippingLog*** to ***mychannel***:

> $ peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --peerAddresses localhost:7051 --tlsRootCertFiles ${PEER0_ORG1_CA} --peerAddresses localhost:9051 --tlsRootCertFiles ${PEER0_ORG2_CA} --channelID mychannel --name shippingLog -v 0 --sequence 1 --tls --cafile $ORDERER_CA --waitForEvent

Once the chaincode definition has been committed to the channel, The chaincode container will start.

2.         Use the ***docker ps*** command to see ***shippingLog*** container starting on both peers.

> $ docker ps

Now that we have deployed the **_papercontract_** chaincode to the channel, we are ready to start using the MagnetoCorp application to issue the commercial paper. This will be discussed in the next step.
