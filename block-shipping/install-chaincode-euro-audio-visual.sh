#!/bin/bash

cd ./organization/euro-audio-visual/
peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0  && \
source euro-audio-visual.sh  && \
peer lifecycle chaincode install cp.tar.gz  && \
PACKAGE_ID=$(peer lifecycle chaincode queryinstalled -O json | jq .installed_chaincodes[0].package_id -r)  && \
echo $PACKAGE_ID
peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name shippingLog -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA


