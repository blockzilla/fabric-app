#!/bin/bash
cd ./organization/shenzhen-shipping/
peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0
source shenzhen-shipping.sh
peer lifecycle chaincode install cp.tar.gz
PACKAGE_ID=$(peer lifecycle chaincode queryinstalled -O json | jq .installed_chaincodes[0].package_id -r)
echo $PACKAGE_ID
peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name shippingLog -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA


peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride \
    orderer.example.com --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ORG1_CA \
    --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_ORG2_CA --channelID mychannel \
    --name shippingLog -v 0 --sequence 1 --tls --cafile $ORDERER_CA --waitForEvent
