cd ./organization/digibank/

source digibank.sh

peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride \
    orderer.example.com --peerAddresses localhost:7051 --tlsRootCertFiles ${PEER0_ORG1_CA} \
    --peerAddresses localhost:9051 --tlsRootCertFiles ${PEER0_ORG2_CA} --channelID mychannel \
    --name shippingLog -v 0 --sequence 2 --tls --cafile $ORDERER_CA --waitForEvent

