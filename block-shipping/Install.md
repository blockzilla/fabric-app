# Workflow chart

https://lucid.app/lucidchart/invitations/accept/inv_47c93df3-328e-462e-885c-08cda08e81a1?viewport_loc=-414%2C-322%2C2720%2C1321%2C0_0

```
export PATH=~/github/colledge/fabric-samples/bin:$PATH
export FABRIC_CFG_PATH=~/github/colledge/fabric-samples/config/
```

# Install Smart contract

```
./network-clean.sh && ./network-starter.sh && ./monitor.sh
./install-chaincode-magnetocorp.sh && ./install-chaincode-digibank.sh
```

# magnetocorp app install

Install node modules, register user and submit issue

```
cd commercial-paper/organization/magnetocorp/application/
npm install
node enrollUser.js
node issue.js
```

# digibank app install

Install node modules, register user and submit issue

```
cd commercial-paper/organization/magnetocorp/application/
npm install
node enrollUser.js
node buy.js
node redeem.js
```
